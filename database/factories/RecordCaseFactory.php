<?php

namespace Database\Factories;

use App\Models\Diagnosis;
use App\Models\Patient;
use App\Models\RecordCase;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecordCaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RecordCase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'diagnosis_id' => Diagnosis::factory(),
            'patient_id' => Patient::factory(),
        ];
    }
}
