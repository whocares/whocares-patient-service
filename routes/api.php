<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function () {
    Route::group(['prefix' => 'patients'], function () {
        Route::get('/', 'PatientController@index');
        Route::post('/', 'PatientController@create');
        Route::get('{patient}', 'PatientController@show');
        Route::get('/user/{userId}', 'PatientController@getByUserId');
    });

    Route::group(['prefix' => 'record-cases'], function () {
        Route::get('/', 'RecordCaseController@index');
        Route::get('/patient/{patient}', 'RecordCaseController@indexByPatient');
        Route::get('/{recordCase}', 'RecordCaseController@show');
    });
});
