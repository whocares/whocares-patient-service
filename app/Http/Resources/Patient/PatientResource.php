<?php

namespace App\Http\Resources\Patient;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'gender' => $this->gender,
            'first_name' => $this->first_name,
            'last_name_prefix' => $this->last_name_prefix,
            'last_name' => $this->last_name,
            'birthdate' => $this->birthdate,
            'citizen_service_number' => $this->citizen_service_number,
            'updated_at' => $this->updated_at->toIso8601String(),
            'created_at' => $this->created_at->toIso8601String(),
            'firebase_user_id' => $this->firebase_user_id,
        ];
    }
}
