<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Patient\PatientResource;
use App\Http\Resources\RecordCase\RecordCaseCollection;
use App\Http\Resources\RecordCase\RecordCaseResource;
use App\Models\Patient;
use App\Models\RecordCase;
use Illuminate\Http\Request;

class RecordCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return RecordCaseCollection
     */
    public function index(): RecordCaseCollection
    {
        $recordCases = RecordCase::query()->paginate();

        RecordCaseCollection::wrap('record_cases');
        return new RecordCaseCollection($recordCases);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Patient  $patient
     * @return RecordCaseCollection
     */
    public function indexByPatient(Patient $patient): RecordCaseCollection
    {
        $recordCases = $patient->recordCases()->paginate();

        RecordCaseCollection::wrap('record_cases');
        return new RecordCaseCollection($recordCases);
    }

    /**
     * Get a record case by its ID.
     *
     * @param  RecordCase  $recordCase
     * @return RecordCaseResource
     */
    public function show(RecordCase $recordCase): RecordCaseResource
    {
        $recordCase->load('recordElements');

        RecordCaseResource::wrap('record_case');
        return new RecordCaseResource($recordCase);
    }
}
