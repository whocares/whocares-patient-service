<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gender' => 'required|in:F,M',
            'first_name' => 'required',
            'last_name_prefix' => 'nullable',
            'last_name' => 'required',
            'birthdate' => 'required|date',
            'firebase_user_id' => 'required',
        ];
    }
}
