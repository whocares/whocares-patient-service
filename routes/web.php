<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api', 'namespace' => '\App\Http\Controllers\Api'], function () {

    Route::group(['prefix' => 'patients'], function () {
        Route::get('/', 'PatientController@index');
        Route::get('{patient}', 'PatientController@show');
    });

    Route::group(['prefix' => 'record-cases'], function () {
        Route::get('/', 'RecordCaseController@index');
        Route::get('/patient/{patient}', 'RecordCaseController@indexByPatient');
    });

});
