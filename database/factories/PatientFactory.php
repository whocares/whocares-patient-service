<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'gender' => $this->faker->randomElement($array = ['M', 'F', 'X']),
            'first_name' => $this->faker->firstName,
            'last_name_prefix' => '',
            'last_name' => $this->faker->lastName,
            'birthdate' => $this->faker->date(),
            'citizen_service_number' => $this->faker->randomNumber($nbDigits = 9),
        ];
    }
}
