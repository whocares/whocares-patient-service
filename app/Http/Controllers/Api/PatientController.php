<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientStoreRequest;
use App\Http\Resources\Patient\PatientCollection;
use App\Http\Resources\Patient\PatientResource;
use App\Models\Patient;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return PatientCollection
     */
    public function index()
    {
        $patients = Patient::query()->paginate();

        PatientCollection::wrap('patients');
        return new PatientCollection($patients);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Patient  $patient
     * @return PatientResource
     */
    public function show(Patient $patient)
    {
        PatientResource::wrap('patient');
        return new PatientResource($patient);
    }

    /**
     * Get a patient by its user ID.
     *
     * @param  String  $userID
     * @return PatientResource
     */
    public function getByUserId(String $userID): PatientResource
    {
        $patient = Patient::where([
            'firebase_user_id' => $userID
        ])->first();

        PatientResource::wrap('patient');
        return new PatientResource($patient);
    }

    /**
     * Create a new patient.
     *
     * @param  PatientStoreRequest  $request
     * @return PatientResource
     */
    public function create(PatientStoreRequest $request): PatientResource
    {
        $patient = $request->validated();
        $patient['citizen_service_number'] = $this->generateRandomString();

        $createdPatient = Patient::create($patient);

        PatientResource::wrap('data');
        return new PatientResource($createdPatient);
    }

    /**
     * Generate a random string.
     *
     * @param  int  $length
     * @return false|string
     */
    private function generateRandomString($length = 9)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 1, $length);
    }
}
