<?php

namespace Database\Factories;

use App\Models\Medication;
use App\Models\Medicine;
use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medication::class;

    /**
     * A list of usage types.
     *
     * @var string
     */
    private $usageTypes = [
        "two time a day",
        "two times a week",
        "once per day",
        "daily at breakfast",
        "once a week",
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'patient_id' => Patient::factory(),
            'medicine_id' => Medicine::factory(),
            'amount' => $this->faker->numberBetween(1, 100),
            'usage_type' => $this->faker->randomElement($array = $this->usageTypes),
            'dosage' => $this->faker->numberBetween(10, 100)  . 'mG',
            'start_date' => $this->faker->date($min = 'now'),
            'end_date' => $this->faker->date($min = 'now'),
        ];
    }
}
