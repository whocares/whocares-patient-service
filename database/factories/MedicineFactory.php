<?php

namespace Database\Factories;

use App\Models\Medicine;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medicine::class;

    /**
     * A list of diseases.
     *
     * @var string
     */
    private $medicines = [
        "Acetaminophen",
        "Adderall",
        "Amitriptyline",
        "Amlodipine",
        "Amoxicillin",
        "Ativan",
        "Atorvastatin",
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement($array = $this->medicines) . ' ' . ucfirst($this->faker->word()),
            'description' => $this->faker->sentence(),
        ];
    }
}
