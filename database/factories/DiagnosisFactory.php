<?php

namespace Database\Factories;

use App\Models\Diagnosis;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiagnosisFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Diagnosis::class;

    /**
     * A list of diseases.
     *
     * @var string
     */
    private $diseases = [
        "Corona",
        "Multiple Sclerosis",
        "Crohn's",
        "Colitis",
        "Lupus",
        "Asthma",
        "Scleroderma",
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement($array = $this->diseases) . ' ' . ucfirst($this->faker->word()),
            'description' => $this->faker->sentence(),
        ];
    }
}
