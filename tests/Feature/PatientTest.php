<?php

namespace Tests\Feature;

use App\Http\Requests\PatientStoreRequest;
use App\Models\HealthcareProvider;
use App\Models\Patient;
use Database\Factories\PatientFactory;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PatientTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function testIndex() {
        Patient::factory()->create();

        $response = $this->get('/api/v1/patient-service/patients');
        $response->assertStatus(200);

        $data = json_decode($response->content(), true);
        $this->assertArrayHasKey('patients', $data);
        $this->assertArrayHasKey('meta', $data);
        $this->assertArrayHasKey('links', $data);

    }

    public function testCreate() {
        $response = $this->postJson('/api/v1/patient-service/patients/', ['gender' => 'F', 'first_name' => 'test', 'last_name' => 'test', 'birthdate' => '2020-01-01', 'firebase_user_id' => '1']);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id', 'first_name', 'last_name', 'gender', 'citizen_service_number', 'birthdate', 'updated_at', 'created_at', 'firebase_user_id', 'last_name_prefix'
            ]
        ]);
    }

    public function testGetByUserId() {
        $userId = '12345';
        $patient = Patient::factory()->create();
        DB::update('update patients SET firebase_user_id = ? where id = ?', [$userId, $patient->id]);

        $response = $this->get('/api/v1/patient-service/patients/user/' . $userId);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'patient' => [
                'id', 'first_name', 'last_name'
            ]
        ]);
    }
}
