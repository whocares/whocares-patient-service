<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RecordCase extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'diagnosis_id',
        'patient_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the diagnosis that belongs to the record case.
     */
    public function diagnosis()
    {
        return $this->belongsTo('App\Models\Diagnosis');
    }

    /**
     * Get the patient that belongs to the record case.
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * Get the record elements that belongs to the record case.
     */
    public function recordElements()
    {
        return $this->hasMany('App\Models\RecordElement');
    }

    /**
     * Get all medications that belong to this record case
     */
    public function medications()
    {
        $recordElementIds = $this->recordElements()->pluck('id');

        return Medication::whereHas('recordElements', function (Builder $query) use ($recordElementIds) {
            $query->whereIn('id', $recordElementIds)->where('element_type', '=', RecordElement::ELEMENT_TYPE_MEDICATION);
        })->with('medicine')->get();
    }

    public function appointments()
    {
        return $this->recordElements()->where('element_type', '=', RecordElement::ELEMENT_TYPE_APPOINTMENT)->get();
    }
}
