<?php

namespace App\Http\Resources\RecordCase;

use App\Http\Resources\Diagnosis\DiagnosisResource;
use App\Http\Resources\RecordElement\RecordElementCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class RecordCaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'diagnosis_id' => $this->diagnosis_id,
            'diagnosis' => new DiagnosisResource($this->diagnosis),
            'patient_id' => $this->patient_id,
            'record_elements' => new RecordElementCollection($this->whenLoaded('recordElements')),
            'updated_at' => $this->updated_at->toIso8601String(),
            'created_at' => $this->created_at->toIso8601String(),
            'medications' => $this->medications(),
            'appointments' => $this->appointments(),
        ];
    }
}
