<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Patient extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender',
        'first_name',
        'last_name_prefix',
        'last_name',
        'birthdate',
        'citizen_service_number',
        'firebase_user_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthdate',
        'created_at',
        'updated_at',
    ];

    public function setCitizenServiceNumber()
    {
        $this->attributes['citizen_service_number'] = $this->generateRandomString();
    }

    /**
     * Get the medications that belong to the patient.
     */
    public function medications()
    {
        return $this->hasMany('App\Models\Medication');
    }

    /**
     * Get the record cases that belong to the patient.
     */
    public function recordCases()
    {
        return $this->hasMany('App\Models\RecordCase');
    }

    function generateRandomString($length = 9)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789', ceil($length / strlen($x)))), 1, $length);
    }
}
