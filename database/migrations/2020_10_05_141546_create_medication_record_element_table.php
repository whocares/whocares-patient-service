<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicationRecordElementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medication_record_element', function (Blueprint $table) {
            $table->uuid('medication_id');
            $table->foreign('medication_id')->references('id')->on('medications');
            $table->uuid('record_element_id');
            $table->foreign('record_element_id')->references('id')->on('record_elements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medication_record_element', function (Blueprint $table) {
            $table->dropForeign(['medication_id']);
            $table->dropForeign(['record_element_id']);
        });

        Schema::dropIfExists('medication_record_element');
    }
}
