<?php

namespace Database\Seeders;

use App\Models\Medication;
use App\Models\Patient;
use App\Models\RecordCase;
use App\Models\RecordElement;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        RecordCase::factory()
            ->has(
                RecordElement::factory()->count(3)->has(Medication::factory(), 'medications'),
                'recordElements'
            )
            ->for(
                Patient::factory()->has(
                    Medication::factory()->count(5)
                )
            )
            ->count(5)
            ->create();
    }
}
