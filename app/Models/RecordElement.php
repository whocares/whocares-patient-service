<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RecordElement extends Model
{
    use HasFactory;

    const ELEMENT_TYPE_MEDICATION = 1;
    const ELEMENT_TYPE_APPOINTMENT = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'record_case_id',
        'element_type',
        'content',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get the record case that belongs to the record element.
     */
    public function recordCase()
    {
        return $this->belongsTo('App\Models\RecordCase');
    }

    /**
     * Get the medications that belong to the record element.
     */
    public function medications()
    {
        return $this->belongsToMany('App\Models\Medication');
    }
}
