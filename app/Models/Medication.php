<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Medication extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patient_id',
        'medicine_id',
        'amount',
        'usage_type',
        'dosage',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];

    /**
     * Get the medicine that belongs to the medication.
     */
    public function medicine()
    {
        return $this->belongsTo('App\Models\Medicine', 'medicine_id');
    }

    /**
     * Get the patient that belongs to the medication.
     */
    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    /**
     * Get the record elements that belong to the medication.
     */
    public function recordElements()
    {
        return $this->belongsToMany('App\Models\RecordElement');
    }
}
