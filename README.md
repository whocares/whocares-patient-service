# Whocares patient service
# 1

## Installation with docker
  * Make sure you have docker installed on your computer
  * Navigate to this folder and run `docker-compose up -d`. This will build the docker images and containers (This might take a while)
  * Next up run the following command: `docker-compose exec application bash`. With this command you will ssh into your laravel container where your application lives.
  * Run `composer install` to install all dependencies
  * Run `cp .env.example .env` to create the environment file
  * Run `php artisan key:generate` to create a key for your application
  * Run `php artisan migrate` to fill your database with tables.
  * Run `php artisan db:seed` to add data to your database.
  * If everything went well you should be able to navigate to `http://localhost` in your browser and see a laravel start screen.  
