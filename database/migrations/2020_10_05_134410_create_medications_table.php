<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->uuid('medicine_id');
            $table->foreign('medicine_id')->references('id')->on('medicines');
            $table->integer('amount');
            $table->string('usage_type');
            $table->string('dosage');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medications', function (Blueprint $table) {
            $table->dropForeign(['patient_id']);
            $table->dropForeign(['medicine_id']);
        });

        Schema::dropIfExists('medications');
    }
}
