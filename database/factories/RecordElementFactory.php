<?php

namespace Database\Factories;

use App\Models\RecordCase;
use App\Models\RecordElement;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecordElementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RecordElement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'record_case_id' => RecordCase::factory(),
            'element_type' => $this->faker->numberBetween(1, 3),
            'content' => $this->faker->text,
        ];
    }
}
