<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_cases', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('diagnosis_id');
            $table->foreign('diagnosis_id')->references('id')->on('diagnoses');
            $table->uuid('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_cases', function (Blueprint $table) {
            $table->dropForeign(['diagnosis_id']);
            $table->dropForeign(['patient_id']);
        });

        Schema::dropIfExists('record_cases');
    }
}
