<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_elements', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('record_case_id');
            $table->foreign('record_case_id')->references('id')->on('record_cases');
            $table->tinyInteger('element_type');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('record_elements', function (Blueprint $table) {
            $table->dropForeign(['record_case_id']);
        });

        Schema::dropIfExists('record_elements');
    }
}
