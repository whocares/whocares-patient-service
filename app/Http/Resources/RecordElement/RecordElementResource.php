<?php

namespace App\Http\Resources\RecordElement;

use App\Http\Resources\Medication\MedicationCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class RecordElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'record_case_id' => $this->record_case_id,
            'element_type' => $this->element_type,
            'content' => $this->content,
            'medications' => new MedicationCollection($this->medications),
            'updated_at' => $this->updated_at->toIso8601String(),
            'created_at' => $this->created_at->toIso8601String(),
        ];
    }
}
