<?php

namespace App\Http\Resources\Medication;

use App\Http\Resources\Medicine\MedicineResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MedicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'patient_id' => $this->patient_id,
            'medicine' => new MedicineResource($this->medicine),
            'medicine_id' => $this->medicine_id,
            'amount' => $this->amount,
            'usage_type' => $this->usage_type,
            'dosage' => $this->dosage,
            'start_date' => isset($this->start_date) ? $this->start_date->toIso8601String() : null,
            'end_date' => isset($this->end_date) ? $this->end_date->toIso8601String() : null,
            'updated_at' => $this->updated_at->toIso8601String(),
            'created_at' => $this->created_at->toIso8601String(),
        ];
    }
}
